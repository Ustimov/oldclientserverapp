package org.ustimov;

import java.util.*;

class Sm implements Smthable {

}

interface Smthable {
    int i = 1;
}

public class UServerMain {

    final static protected   class Klass {

    }



    public static void main(String[] args) throws Exception {
        /*
        UServer server = new UServer(1234);
        server.start();
        System.out.println("Server is started. Press ENTER to stop.");
        System.in.read();
        server.stop();
        Thread.sleep(1000);
        System.out.println("Server was stopped.");
        */
        /*
        Sm sm = new Sm();
        ArrayList<Sm> list = new ArrayList<>();
        list.add(sm);
        smth(list);
        */
        List<Object> l = new ArrayList<>();
        l.add(1);
        l.add("chrchr");
        for (Object obj : l) {
            System.out.println(obj);
        }
    }

    static void smth(List<? extends Smthable> list) {
        for (Smthable s : list) {
            System.out.print(s.i);
        }
    }

    final void up() {

    }

    public class User
    {
        public int SignalId ;

        public int SessionId ;


        public String TagName ;

        public double Value ;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            User user = (User) o;

            if (SignalId != user.SignalId) return false;
            if (SessionId != user.SessionId) return false;
            if (Double.compare(user.Value, Value) != 0) return false;
            return !(TagName != null ? !TagName.equals(user.TagName) : user.TagName != null);

        }

        @Override
        public int hashCode() {
            int result;
            long temp;
            result = SignalId;
            result = 31 * result + SessionId;
            result = 31 * result + (TagName != null ? TagName.hashCode() : 0);
            temp = Double.doubleToLongBits(Value);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            return result;
        }
    }
}

