package org.ustimov;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.ConcurrentLinkedQueue;

public class UServer {
    private ServerSocket serverSocket = null;

    private ConcurrentLinkedQueue<Socket> clients = new ConcurrentLinkedQueue<>();

    private boolean isRunning = true;

    public UServer(int port) {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            System.out.println("[Exception 1] " + e.getMessage());
        }
    }

    public void start() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (isRunning) {
                    try {
                        Socket client = serverSocket.accept();
                        clients.add(client);
                        System.out.println("Client is connected.");
                    } catch (IOException e) {
                        System.out.println("[Exception 2] " + e.getMessage());
                    }
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (isRunning) {
                    Socket client = clients.poll();
                    if (client == null) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            System.out.println("[Exception 3] " + e.getMessage());
                        }
                    } else {
                        try {
                            Calendar calendar = GregorianCalendar.getInstance();
                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
                            objectOutputStream.writeObject(calendar);
                            client.getOutputStream().write(byteArrayOutputStream.toByteArray());
                            System.out.println("Response for client was sent.");
                        } catch (IOException e) {
                            System.out.println("[Exception 4] " + e.getMessage());
                        }
                    }
                }
            }
        }).start();
    }

    public void stop() {
        try {
            serverSocket.close();
        } catch (IOException e) {
            System.out.println("[Exception 5] " + e.getMessage());
        }
        isRunning = false;
    }

    public boolean isRunning() {
        return isRunning;
    }
}
