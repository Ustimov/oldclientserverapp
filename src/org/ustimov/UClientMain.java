package org.ustimov;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class UClientMain {
    public static void main(String[] args) {
        try {

            System.out.println("Choose option:\n1. Date\n2. Time\n3. Date and time\nAnd other NUMBER to exit.");

            Scanner scanner = new Scanner(System.in);
            int choice = scanner.nextInt();

            UClient client = new UClient("localhost", 1234);
            Date dateTime = client.getDateTime().getTime();

            switch (choice) {
                case 1:
                    System.out.println("Date: " + new SimpleDateFormat("dd.MM.yyyy").format(dateTime));
                    break;
                case 2:
                    System.out.println("Time: " + new SimpleDateFormat("HH:mm:ss").format(dateTime));
                    break;
                case 3:
                    System.out.println("Date and time: " + new SimpleDateFormat("dd.MM.yyyy HH:mm:ss")
                            .format(dateTime));
            }

        } catch (Exception e) {
            System.out.println("[Exception] " + e.getMessage());
        }
    }
}
