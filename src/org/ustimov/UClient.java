package org.ustimov;

import java.io.*;
import java.net.Socket;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class UClient {
    private String host = null;
    private int port;

    public UClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public Calendar getDateTime() throws Exception {
        Socket socket = new Socket(host, port);
        InputStream inputStream = socket.getInputStream();

        while (inputStream.available() == 0) {
            Thread.sleep(100);
        }

        int inputLength = inputStream.available();
        byte[] buffer = new byte[inputLength];
        inputStream.read(buffer);

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buffer);
        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        Calendar calendar = (GregorianCalendar)objectInputStream.readObject();

        socket.close();
        return calendar;
    }
}
